package ru.trippel.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.ITerminalService;
import ru.trippel.tm.api.service.IUserService;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ITerminalService getTerminalService();

}
