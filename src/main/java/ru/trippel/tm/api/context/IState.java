package ru.trippel.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import java.util.List;

public interface IState {

    @NotNull
    User getCurrentUser();

    @NotNull
    List<AbstractCommand> getCommands();

    void setCurrentUser(@NotNull User user);

}
