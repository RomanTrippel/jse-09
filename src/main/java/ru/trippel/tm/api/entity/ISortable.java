package ru.trippel.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.enumeration.Status;
import java.util.Date;

public interface ISortable {

    @NotNull
    Date getDateStart();

    @NotNull
    Date getDateFinish();

    @NotNull
    Status getStatus();

}
