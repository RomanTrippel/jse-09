package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;
import java.util.List;

public final class ProjectViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_VIEW";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @NotNull final SortingMethod sortingMethod = currentUser.getProjectSortingMethod();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId, sortingMethod);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i));
        }
    }

}