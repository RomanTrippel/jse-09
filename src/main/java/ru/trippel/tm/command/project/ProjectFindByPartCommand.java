package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import java.util.List;

public final class ProjectFindByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_FINDBYPART";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a project by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter the part of the name or description to search.");
        @NotNull final String searchText = serviceLocator.getTerminalService().read();
        @Nullable final List<Project> foundProjectList = serviceLocator.getProjectService().
                findByPart(userId, searchText);
        System.out.println("List of found projects:");
        if (foundProjectList != null) {
            if (foundProjectList.size() != 0) {
                for (@NotNull final Project project : foundProjectList) System.out.println(project);
            }
        }
        System.out.println("Search for matches is over.");
    }

}
