package ru.trippel.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import java.util.List;

public final class TaskFindByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "TASK_FINDBYPART";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a task by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter the part of the name or description to search.");
        @NotNull final String searchText = serviceLocator.getTerminalService().read();
        @Nullable final List<Task> foundTaskList = serviceLocator.getTaskService().
                findByPart(userId, searchText);
        System.out.println("List of found tasks:");
        if (foundTaskList != null) {
            if (foundTaskList.size() != 0) {
                for (@NotNull final Task task : foundTaskList) System.out.println(task);
            }
        }
        System.out.println("Search for matches is over.");
    }

}
