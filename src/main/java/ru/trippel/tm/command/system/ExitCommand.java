package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "EXIT";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit the application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
