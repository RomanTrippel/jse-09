package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;

@NoArgsConstructor
public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "LOGOUT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign out of account.";
    }

    @Override
    public void execute() {
        state.setCurrentUser(new User());
        System.out.println("You are signed out of your account.");
    }

}
