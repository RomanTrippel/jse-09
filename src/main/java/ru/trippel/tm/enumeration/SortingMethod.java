package ru.trippel.tm.enumeration;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum SortingMethod {

    @NotNull
    CREATION_ORDER("In the order of creation."),
    @NotNull
    START_DATE("By start date."),
    @NotNull
    FINISH_DATE("By finish date."),
    @NotNull
    STATUS("By status.");

    @Getter
    @NotNull
    private String displayName = "";

    SortingMethod(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
